function Camera(x, y, width, height) {
    this.coords = Physics.vector(x, y);
    this.viewport = Physics.vector(width, height);

    this.move = function (dir) {
        this.coords.vadd(dir);
    };

    this.isVisible = function (entity) {
        var bbox = entity.physBody.geometry.aabb();
        bbox.top = entity.physBody.state.pos.clone().vsub(Physics.vector(bbox.hw, bbox.hh));
        bbox.bottom = entity.physBody.state.pos.clone().vadd(Physics.vector(bbox.hw, bbox.hh));
        bbox.top.vadd(Physics.vector(bbox.x, bbox.y));
        bbox.bottom.vadd(Physics.vector(bbox.x, bbox.y));
        var d = this.coords.clone().vadd(this.viewport);
        var f1 = bbox.top.x > this.coords.x && bbox.top.x < d.x;
        var f2 = bbox.bottom.x > this.coords.x && bbox.bottom.x < d.x;
        var f3 = bbox.top.y > this.coords.y && bbox.top.y < d.y;
        var f4 = bbox.bottom.y >= this.coords.y && bbox.bottom.y <= d.y;
        var f5 = bbox.top.x < this.coords.x && bbox.bottom.x > d.x;
        var f6 = bbox.top.y < this.coords.y && bbox.bottom.y > d.y;
        return ((f1 || f2) && (f3 || f4)) || (f5 || f6);
    };
}