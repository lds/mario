/* Abstract Entity */
function Entity(x, y) {
    this.graphics = null;
    this.id = null;
    this.physBody = null;

    /* True if Entity is passable by a player, false otherwise */
    this.checkPassability = function () {
        return !(this instanceof StaticLevelObject && !this.passable);
    };

    this.draw = function () {
        if (this.graphics) {
            this.graphics.draw(this.physBody);
        }
        else
            throw 'Graphics is null.';
    };

    this.move = function () {
        throw 'Not implemented - abstract function';
    };

    this.jsonify = function () {
        if (this.physBody) {
            var data = {
                id: this.id,
                state: this.physBody.state
            };
            return {data: JSON.stringify(data)};
        }
        return {data: null};
    };
}

/* Part of a level */
function LevelObject(x, y) {
    Entity.call(this, x, y);
}
LevelObject.prototype = new Entity();

/* Immovable non-collideable level object, without physics interaction */
function StaticLevelObject(x, y) {
    LevelObject.call(this, x, y);

    this.physBody = Physics.body('convex-polygon', {
        x: x + globals.level.tileSize / 2,
        y: y + globals.level.tileSize / 2,
        treatment: "static",
        mass: 1,
        vertices: [
            { x: 0, y: 0 },
            { x: 0, y: globals.level.tileSize },
            { x: globals.level.tileSize, y: globals.level.tileSize },
            { x: globals.level.tileSize, y: 0 }
        ],
        view: null
    });
}
StaticLevelObject.prototype = new LevelObject();

/* Dynamic level object */
function DynamicLevelObject(x, y, vertices, is_fixed, labels) {
    LevelObject.call(this, x, y);

    this.passable = false;
    this.physBody = Physics.body('convex-polygon', {
        x: x,
        y: y,
        treatment: is_fixed ? "static" : "dynamic",
        mass: 1,
        vertices: vertices,
        view: null,
        labels: labels !== undefined ? labels : []
    });

    var cc = Physics.geometry.getPolygonCentroid(vertices);
    this.physBody.state.pos.add(cc.x, cc.y);
    this.physBody.recalc();
}
DynamicLevelObject.prototype = new LevelObject();

/* Player associated content */
function Player(x, y, vertices) {
    Entity.call(this, x, y);
    this.id = null; // ID used to identify player on server side
    this.physBody = Physics.body('convex-polygon', {
        x: x,
        y: y,
        treatment: "dynamic",
        mass: 1,
        restitution: 0,
        vertices: vertices,
        view: null,
        labels: ['player']
    });

    var cc = Physics.geometry.getPolygonCentroid(vertices);
    this.physBody.state.pos.add(cc.x, cc.y);
    this.physBody.recalc();

    this.move = function (dir) {
        this.physBody.state.vel = Physics.vector(dir.x / 128, dir.y / 128);
    };

    this.health = 100;
    this.currentScore = 0;
    this.bestScore = 0;
    this.immune = 0; // the amount of time in ms during which player is immune to damage
    this.kick = function(vec) {
        this.physBody.applyForce(vec);
        this.immune = 1000;
    };

    this.updateStats = function(dt) {
      this.immune = Math.max(0, this.immune - dt);
    };
}
Player.prototype = new Entity();