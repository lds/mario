var canvas = document.getElementById('canvas');
var context = canvas.getContext('2d');

canvas.width = $(canvas).width();
canvas.height = $(canvas).height();
canvas.offsetLeft = 0;
canvas.offsetTop = 0;

var GraphicsTypes = {COLOR: 0, TILES: 1};

function Graphics(type, data) {
    this.type = type;
    this.color = data * '0f0';
    this.tiles = [];
    this.draw = function (body) {
        var pos = body.state.pos.clone().vsub(globals.camera.coords);
        context.save();
        context.translate(pos.x, pos.y);
        context.rotate(body.state.angular.pos);

        switch (type) {
            case GraphicsTypes.COLOR:
                context.fillStyle = '#' + this.color;
                context.beginPath();
                var vert = body.geometry.vertices;
                for (var i = 0; i < vert.length; i++) {
                    context.lineTo(vert[i].x, vert[i].y);
                }
                context.closePath();
                context.fill();
                break;
            case GraphicsTypes.TILES:
                var tileSize = globals.level.tileSize;
                for (var j = 0; j < this.tiles.length; ++j) {
                    var t = this.tiles[j];
                    var tileSet = globals.level.tileSets[t.tileSetID];
                    var tileSetWidth = Math.round(tileSet.width / tileSize);
                    var tx = ((t.tileID - tileSet.firstgid) % tileSetWidth) * tileSize, ty = Math.floor((t.tileID - tileSet.firstgid) / tileSetWidth) * tileSize;
                    context.drawImage(tileSet, tx, ty, tileSize, tileSize, t.x, t.y, tileSize, tileSize);
                }
                break;
            default:
                console.log('Not implemented');
        }
        context.restore();
    };

    // optional init data can be passed
    if (data !== undefined) {
        switch (type) {
            case GraphicsTypes.COLOR:
                this.color = data;
                break;
            case GraphicsTypes.TILES:
                this.tiles = data;
                break;
            default:
                console.log('Not implemented');
        }
    }
}

function fillLevelBackground() {
    context.clearRect(0, 0, canvas.width, canvas.height);

    context.save();
    context.translate(-globals.camera.coords.x, -globals.camera.coords.y);
    context.fillStyle = '#eee';
    context.fillRect(   0,
                        0,
                        globals.level.width * globals.level.tileSize,
                        globals.level.height * globals.level.tileSize);
    context.restore();
}

function drawCircle(pos, radius, color) {
    context.beginPath();
    context.arc(pos.x, pos.y, radius, 0, 2 * Math.PI, false);
    context.fillStyle = '#' + color;
    context.fill();
    context.stroke();
}

function drawRect(pos, size, color) {
    context.fillStyle = '#' + color;
    context.fillRect(pos.x, pos.y, size.x, size.y);
}

function numberToColor(num) {
    return (num % 16581375).toString(16);
}