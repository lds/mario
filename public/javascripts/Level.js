function Level(tmxString, playerName) {
    this.entities = [];
    this.width = 0; // in tiles
    this.height = 0; // in tiles
    this.tileSize = 0; // in pixels
    this.tileSets = [];

    this.addEntity = function (entity) {
        this.entities.push(entity);
        if (entity instanceof DynamicLevelObject || entity instanceof Player)
            globals.world.addBody(entity.physBody);
    };

    this.removeEntity = function (entity) {
        var index = this.entities.indexOf(entity);
        if (index > -1)
            this.entities.splice(index, 1);
        if (entity instanceof DynamicLevelObject || entity instanceof Player)
            globals.world.removeBody(entity.physBody); //remove any sign of physical existence
    };

    this.removeEntityByUID = function (id) {
        var ent = this.getEntityByBodyUID(id);
        if (ent)
            this.removeEntity(ent);
    };

    this.init = function(level, tmxString, playerName) {
        if (tmxString == undefined)
            return;

        var tmxObject = JSON.parse(tmxString);
        globals.level = level;
        level.tileSize = tmxObject.tileheight;  // assume tileheight == tilewidth
        level.width = tmxObject.width;
        level.height = tmxObject.height;

        for (var h = 0; h < tmxObject.tilesets.length; ++h) {
            level.tileSets.push(new Image());
            level.tileSets[h].src = tmxObject.tilesets[h].image.replace('..\/..\/resources\/', '/images/');
            level.tileSets[h].firstgid = tmxObject.tilesets[h].firstgid;
        }

        var getTileSetIndex = function(tileID) {
            if (level.tileSets.length == 1)
                return 0;

            for (var h = 1; h <  level.tileSets.length; ++h) {
                if (level.tileSets[h].firstgid > tileID)
                    return h - 1;
            }

            return level.tileSets.length - 1;
        };


        var player;
        // start with creating physics objects
        for (var i = 0; i < tmxObject.layers.length; ++i) {
            var lvl = tmxObject.layers[i];
            if (lvl.type != "objectgroup")
                continue;

            for (var j = 0; j < lvl.objects.length; ++j) {
                var obj_descr = lvl.objects[j];
                var vertices = [];
                if (obj_descr.polygon) {
                    vertices = obj_descr.polygon;
                }
                else if (obj_descr.polyline) {
                    vertices = obj_descr.polyline;
                    vertices.pop();
                }
                else if (obj_descr.ellipse)
                    vertices = [
                        { x: 0, y: 0 },
                        { x: 0, y: obj_descr.height },
                        { x: obj_descr.width, y: obj_descr.height },
                        { x: obj_descr.width, y: 0 }
                    ];
                else // rectangular
                    vertices = [
                        { x: 0, y: 0 },
                        { x: 0, y: obj_descr.height },
                        { x: obj_descr.width, y: obj_descr.height },
                        { x: obj_descr.width, y: 0 }
                    ];

                var object;
                try {
                    if (obj_descr.type == "player")
                        object = new Player(obj_descr.x, obj_descr.y, vertices);
                    else {
                        var is_static = obj_descr.type != 'coin';
                        object = new DynamicLevelObject(obj_descr.x, obj_descr.y, vertices, is_static, [obj_descr.type, lvl.name]);
                    }
                }
                catch (e) {
                    console.log(e, obj_descr);
                }

                object.graphics = new Graphics(GraphicsTypes.TILES);
                //object.graphics = new Graphics(GraphicsTypes.COLOR);
                object.graphics.color = 'eff';
                this.addEntity(object);

                if (obj_descr.type == "player") {
                    player = object;
                }
            }
        }

        // then bind tiles to physics objects if any or create non-physics decorations
        for (i = 0; i < tmxObject.layers.length; ++i) {
            lvl = tmxObject.layers[i];
            if (lvl.type != "tilelayer")
                continue;

            for (j = 0; j < lvl.data.length; ++j) {
                var tileID = lvl.data[j];
                if (tileID == 0)
                    continue;

                var in_object = false;
                var y = Math.floor(j / lvl.width) * level.tileSize, x = (j % lvl.width) * level.tileSize;
                var tileCenter = Physics.vector(x + level.tileSize / 2, y + level.tileSize / 2);
                for (var k = 0; k < this.entities.length; ++k) {
                    var ent = this.entities[k];
                    if (hasLabel(ent.physBody, lvl.name) && (ent instanceof DynamicLevelObject || ent instanceof Player)) {
                        var biasedCenter = tileCenter.clone().vsub(ent.physBody.state.pos);
                        if (Physics.geometry.isPointInPolygon(biasedCenter, ent.physBody.geometry.vertices)) {
                            ent.graphics.tiles.push({
                                "x": x - ent.physBody.state.pos.x,
                                "y": y - ent.physBody.state.pos.y,
                                "tileID": tileID,
                                "tileSetID": getTileSetIndex(tileID)
                            });
                            in_object = true;
                            break;
                        }
                    }
                }
                if (in_object)
                    continue;

                // tile is just a decoration and not binded to physics object
                object = new StaticLevelObject(x, y);
                object.graphics = new Graphics(GraphicsTypes.TILES);
                object.graphics.tiles.push({
                    "x": x - object.physBody.state.pos.x,
                    "y": y - object.physBody.state.pos.y,
                    "tileID": tileID,
                    "tileSetID": getTileSetIndex(tileID)
                });
                this.addEntity(object);
            }
        }

        $.post('/add_player', {name: playerName}, function (data) {
            player.id = data.player.id;
            globals.currentPlayer = player;
        });
    };

    this.init(this, tmxString, playerName);

    this.findByCoords = function(pos) {
        var x = pos.x, y = pos.y;
        for (var i = 0; i < this.entities.length; ++i) {
            var aabb = this.entities[i].physBody.aabb();
            var s = this.entities[i].physBody.state.pos;
            var h = aabb.hh, w = aabb.hw;
            if (s.x - w < x && s.x + w > x && s.y - h < y && s.y + h > y)
                return this.entities[i];
        }
        return null;
    };

    this.getEntityByBodyUID = function (id) {
        for (var i = 0; i < this.entities.length; i++) {
            if (this.entities[i].physBody.uid && this.entities[i].physBody.uid == id)
                return this.entities[i];
        }
        return null;
    };

    this.draw = function () {
        fillLevelBackground();
        var half_viewport = Physics.vector(globals.camera.viewport.x / 2, globals.camera.viewport.y / 2);
        globals.camera.coords = globals.currentPlayer.physBody.state.pos.clone().vsub(half_viewport);
        // limit camera move - x
        globals.camera.coords.x = Math.max(1.0, globals.camera.coords.x);
        globals.camera.coords.x = Math.min(globals.level.width * globals.level.tileSize - canvas.width, globals.camera.coords.x);
        // limit camera moves - y
        globals.camera.coords.y = Math.max(1.0, globals.camera.coords.y);
        globals.camera.coords.y = Math.min(globals.level.height * globals.level.tileSize - canvas.height, globals.camera.coords.y);
        for (var i = 0; i < this.entities.length; i++) {
            var entity = this.entities[i];
            if (globals.camera.isVisible(entity)) {
                entity.draw();
            }
        }
    };
}
