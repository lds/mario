var globals = {
    level: null,
    currentPlayer: null,
    camera: null,
    world: null
};

var hasLabel = function(obj, label) {
    return obj.labels ? obj.labels.indexOf(label) > -1 : false;
};