$(document).ready(function () {
    Physics(function (world) {
        globals.world = world;
    });

    globals.world.on('step', function () {
        if (globals.level && globals.currentPlayer)
            globals.level.draw();
    });

    Physics.util.ticker.on(function (time) {
        globals.world.step(time);
        if (globals.currentPlayer) {
            globals.currentPlayer.updateStats(globals.world.timestep());
            updateStats();
        }
    });

    globals.world.on('collisions:detected', function (data) {
        if (! globals.currentPlayer)
            return;

        data = data.collisions;
        for (var i = 0; i < data.length; ++i) {
            var c = data[i];
            var p = null, b;
            if (hasLabel(c.bodyA, 'player')) {
                p = c.bodyA;
                b = c.bodyB;
            }
            else if (hasLabel(c.bodyB, 'player')) {
                p = c.bodyB;
                b = c.bodyA;
            }
            if (!p)
                continue;

            var ent;
            if (hasLabel(b, 'coin')) {
                ent = globals.level.getEntityByBodyUID(b.uid);
                //console.log('Got coin entity', ent, 'Removed coin on collision.');
                globals.level.removeEntity(ent);
                globals.currentPlayer.currentScore += 1;
            } else if (hasLabel(b, 'spike')) {
                if (! globals.currentPlayer.immune) {
                    globals.currentPlayer.health -= 50;
                    if (globals.currentPlayer.health <= 0) {
                        gameOver(false);
                    }
                    globals.currentPlayer.kick(Physics.vector(0, 0.2));
                }
            } else if (hasLabel(b, 'flag')) {
                if (globals.currentPlayer.currentScore == 12) // all coins collected
                    gameOver(true);
            }
        }
    });
}).keydown(function (e) {
    var dir;
    switch (e.which) {
        case 37: // left arrow
        case 65: // a
            dir = Physics.vector(-1, 0);
            break;
        case 38: // up arrow
        case 87: // w
            dir = Physics.vector(0, -1);
            break;
        case 39: // right arrow
        case 68: // d
            dir = Physics.vector(1, 0);
            break;
        case 40: // down arrow
        case 83: // s
            dir = Physics.vector(0, 1);
            break;
        case 116:
            window.location.reload();
            break;
    }
    if (dir !== undefined) {
        dir.mult(globals.level.tileSize / 2);
        globals.currentPlayer.move(dir);
        e.preventDefault();
    }
});

var e = jQuery.Event("keydown");

$('#up').click(function () {
    e.which = 38;
    $(document).trigger(e);
});

$('#down').click(function () {
    e.which = 40;
    $(document).trigger(e);
});

$('#left').click(function () {
    e.which = 37;
    $(document).trigger(e);
});

$('#right').click(function () {
    e.which = 39;
    $(document).trigger(e);
});

var is_mobile = navigator.userAgent.match(/(iPad)|(iPhone)|(iPod)|(android)|(webOS)/i);
if (is_mobile)
    $('.button-group').css('display', 'block');


$(canvas).click(function (e) {
    var pos = Physics.vector(e.offsetX, e.offsetY);
    pos = pos.vadd(globals.camera.coords);
    var entity = globals.level.findByCoords(pos);
    if (entity)
        console.log('Got something:', entity);
    else
        console.log('Miss!');
});

var lastUpdate = Date.now();
function updateStats() {
    $('.counter').html(globals.currentPlayer.currentScore);
    var health = $('.health');
    if (globals.currentPlayer.health == 100) {
        health.removeClass('half').removeClass('empty');
    } else if (globals.currentPlayer.health < 100 && globals.currentPlayer.health > 0) {
        health.removeClass('empty').addClass('half');
    } else
        health.addClass('empty').removeClass('half');

    var stats = {
        'id': globals.currentPlayer.id,
        'currentScore': globals.currentPlayer.currentScore
    };

    if (Date.now() - lastUpdate > 1000) {
        $.post('/update_player', {data: JSON.stringify(stats)}, function (data) {
            lastUpdate = Date.now();
            getPlayers();
        });
    }
}

function getPlayers() {
    if (! globals.currentPlayer)
        return;

    $.get('/get_all_players', function(data) {
        var pl_list = data.players;
        var currentPlayerID = globals.currentPlayer.id;
        for (var j = 0; j < pl_list.length; ++j) {
            if (pl_list[j].id != currentPlayerID)
                pl_list[j].stats.bestScore = Math.max(pl_list[j].stats.bestScore, pl_list[j].stats.currentScore);
            else
                pl_list[j].stats.bestScore = pl_list[j].stats.currentScore;

            if (isNaN(pl_list[j].stats.bestScore))
                pl_list[j].stats.bestScore = 0;
        }
        pl_list.sort(function(a, b) {
            return b.stats.bestScore - a.stats.bestScore;
        });
        var leaders = $('#leader-list');
        leaders.html('');
        for (var i = 0; i < pl_list.length; ++i) {
            var p = pl_list[i];
            if (! p.stats.name || typeof p.stats.name === 'undefined')
                continue;
            if (p.id == currentPlayerID) {
                leaders.append('<li class="current">' + p.stats.name + '<span>' + p.stats.currentScore + '</span></li>');
                globals.currentPlayer.bestScore = p.stats.bestScore;
            }
            else
                leaders.append('<li>' + p.stats.name + '<span>' + p.stats.bestScore + '</span></li>');
        }
    });
}


var greeting = $('#greeting'), nameInput = $('#playerName'), leaderboard = $('.leaderboard'), gameover = $('#gameover');
leaderboard.hide();
gameover.hide();
$.get('/player_exists', function(data) {
    var status = data.status;
    if (status && data.player) {
        var player = JSON.parse(data.player);
        nameInput.hide();
        greeting.show();
        greeting.html('Welcome back, <span>' + player.name + '</span>!<br>Your best score is <span>' + player.bestScore + '</span>.');
    } else {
        greeting.hide();
    }
});

var loaded = false;
$('#submit-modal').click(function() {
    if (loaded) {
        location.reload();
        return;
    }

    var playerName = nameInput.val();
    $.get('/get_level', function (data) {
        globals.level = new Level(data.map, playerName);
        globals.camera = new Camera(0, 0, canvas.width, canvas.height);
        globals.world.add([
            Physics.behavior('body-collision-detection'),
            Physics.behavior('constant-acceleration'),
            Physics.behavior('body-impulse-response'),
            Physics.behavior('sweep-prune'),
            Physics.behavior('edge-collision-detection', {
                aabb: Physics.aabb(0, 0, globals.level.width * globals.level.tileSize, globals.level.height * globals.level.tileSize),
                restitution: 0.2,
                cof: 0.8
            })
        ]);
        Physics.util.ticker.start();

        leaderboard.show();
        $('.modal').hide();
        loaded = true;
    });
});

function gameOver(win) {
    nameInput.hide();
    greeting.hide();
    gameover.show();
    $('.modal').show();

    if (win) {
        gameover.html('<span>Congratulations!</span><br/>You completed the level.');
    } else {
        gameover.html('<span>Game over!</span><br/>Your score is ' + globals.currentPlayer.currentScore + '.');
    }

    var stats = {
        'id': globals.currentPlayer.id,
        'currentScore': globals.currentPlayer.currentScore,
        'bestScore': Math.max(globals.currentPlayer.currentScore, globals.currentPlayer.bestScore)
    };
    $.post('/update_player', {data: JSON.stringify(stats)}, function (data) {
        lastUpdate = Date.now();
    });
}

function deletePlayers(password) {
    $.post('/delete_players', {password: password})
        .done(function() {
            console.log('Players deleted.');
        })
        .fail(function() {
            console.log('Access denied.');
        });
}