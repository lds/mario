var player = require('player');

/* Renders index page */
exports.index = function (req, res) {
    res.render('index', { title: 'Mario' });
};

/* Returns JSON level description (TMX map) */
exports.getLevel = function (req, res) {
    fs = require('fs');
    fs.readFile('public/images/pretty.json', 'utf8', function (err, data) {
        if (err) {
            return console.log(err);
        }
        res.json({map: data});
    });
};

/* Creates new player and returns it's ID */
exports.addPlayer = function (req, res) {
    var player_id = req.session.player_id;
    if (player_id) {
        var stats = {
            lastActive: Date.now()
        };
        player.updatePlayer(player_id, stats, function(pl) {
            pl.id = player_id;
            res.json({player: pl});
        });
    } else {
        try {
            player.addPlayer(req.body.name, function (pl) {
                req.session.player_id = pl.id;
                res.json({player: pl});
            });
        }
        catch (err) {
            res.send(400, 'Unable to add player.');
        }
    }
};

/* Updates a player by given ID */
exports.updatePlayer = function (req, res) {
    try {
        var data = JSON.parse(req.body.data);
        var stats = {
            currentScore: data.currentScore,
            lastActive: Date.now()
        };
        if (data.bestScore) // present only on gameover
            stats.bestScore = data.bestScore;
        player.updatePlayer(data.id, stats, function() {
            res.send(200);
        });
    }
    catch (err) {
        res.send(400, 'Unable to remove player.');
    }
};

/* Returns JSON-ed list of players */
exports.getAllPlayers = function (req, res) {
    player.getAllPlayers(function(data) {
        res.json({players: data});
    });
};

/* Returns true if player is returning one, false otherwise */
exports.playerExists = function (req, res) {
    var player_id = req.session.player_id;
    if (player_id) {
        player.findPlayer(player_id, function(data) {
            if (! data) {
                delete req.session.player_id;
            }
            res.json({status: data ? true : false, player: data});
        });
    }
    else
        res.json({status: false});
};

exports.deletePlayers = function(req, res) {
    var password = req.body.password;
    if (password != 'top-secret')
        res.send(401);
    player.deletePlayers(function(status) {
        res.send(200);
    });
};